﻿#include <iostream>
#include <string>

template <typename T>
class Stack {
public:
    Stack() {
        capacity_ = 10;
        count_ = 0;
        array_ = new T[capacity_];
    }
    ~Stack() {
        delete[] array_;
        array_ = nullptr;
    }
    void push(T item) {
        if (count_ == capacity_) {
            T* new_array = new T[capacity_ * 2];
            std::copy(array_, array_ + capacity_, new_array);
            capacity_ *= 2;
            delete[] array_;
            array_ = new_array;
        }
        array_[count_] = item;
        count_++;
    }
    T pop() {
        if (count_ > 0) {
            count_--;
            return array_[count_];
        }
        return {};
    }
    bool isEmpty() {
        return count_ == 0;
    }
    void empty() {
        count_ = 0;
        capacity_ = 10;
        delete[] array_;
        array_ = new T[capacity_];
    }
    int getCount() {
        return count_;
    }
private:
    int capacity_;
    int count_;
    T* array_;
};

template <typename T>
struct NodeStack {
    T data;
    NodeStack<T>* next = nullptr;
};

template <typename T>
class Stack2 {
public:
    Stack2() {
        top = nullptr;
        count_ = 0;
    }
    Stack2(const Stack2& stack) = delete;
    Stack2 operator =(const Stack2& stack) = delete;
    ~Stack2() {
        empty();
    }
    void push(T item) {
        auto* new_node = new NodeStack<T>;
        new_node->next = top;
        top = new_node;
        top->data = item;
        count_++;
    }
    T pop() {
        if (top != nullptr) {
            T item = top->data;
            auto* node = top;
            top = top->next;
            delete node;
            return item;
            count_--;
        }
        return {};
    }
    bool isEmpty() {
        return top == nullptr;
    }
    void empty() {
        count_ = 0;
        while (!isEmpty()) {
            pop();
        }
    }
    int getCount() {
        return count_;
    }
private:
    int count_;
    NodeStack<T>* top;
};

int main() {

    //----------------- class Stack ------------------
    std::cout << "Stack of integers." << std::endl;
    Stack<int> stack_test_int;
    for (int i = 0; i < 12; ++i) {
        stack_test_int.push(i);
        std::cout << i << " ";
    }
    std::cout << std::endl;

    std::cout << "Count: " << stack_test_int.getCount() << std::endl;
    std::cout << "Pop: ";
    for (int i = 0; i < 13; ++i) {
        if (!stack_test_int.isEmpty())
            std::cout << stack_test_int.pop() << " ";
    }
    std::cout << std::endl << std::endl;

    std::cout << "Stack of strings." << std::endl;
    Stack<std::string> stack_test_str;
    for (int i = 0; i < 12; ++i) {
        stack_test_str.push(std::to_string(i));
        std::cout << i << " ";
    }
    std::cout << std::endl;

    std::cout << "Count: " << stack_test_str.getCount() << std::endl;
    std::cout << "Pop: ";
    for (int i = 0; i < 13; ++i) {
        if (!stack_test_str.isEmpty())
            std::cout << stack_test_str.pop() << " ";
    }
    std::cout << std::endl << std::endl;

    //----------------- class Stack2 ------------------
    std::cout << "Stack2 of double." << std::endl;
    Stack2<double> stack2_test_double;
    for (int i = 0; i < 5; ++i) {
        stack2_test_double.push(i + 0.5);
        std::cout << i + 0.5 << " ";
    }
    std::cout << std::endl;

    std::cout << "Count: " << stack2_test_double.getCount() << std::endl;
    std::cout << "Pop: ";
    for (int i = 0; i < 7; ++i) {
        if (!stack2_test_double.isEmpty())
            std::cout << stack2_test_double.pop() << " ";
    }
    std::cout << std::endl << std::endl;

    std::cout << "Stack2 of strings." << std::endl;
    Stack2<std::string> stack2_test_str;
    for (int i = 0; i < 6; ++i) {
        stack2_test_str.push("str" + std::to_string(i));
        std::cout << "str" << i << " ";
    }
    std::cout << std::endl;

    std::cout << "Count: " << stack2_test_str.getCount() << std::endl;
    std::cout << "Pop: ";
    for (int i = 0; i < 8; ++i) {
        if (!stack2_test_str.isEmpty())
            std::cout << stack2_test_str.pop() << " ";
    }
    std::cout << std::endl;

    return 0;
}
